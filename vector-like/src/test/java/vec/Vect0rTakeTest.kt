//package cursors.vec
//TODO()
//
//import org.junit.jupiter.api.Test
//import vec.macros.get
//import vec.macros.size
//import vec.macros.take
//import vec.macros.takeLast
//
//import kotlin.test.assertEquals
//
///**
// * Unit test of Vect0r.take and Vect0r.takeLast
// */
//class Vect0rTakeTest {
//    @Test
//    fun testTake() {
//        val v = vec.util._v[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
//        val v1 = v.take(5)
//        assertEquals(v1.size, 5)
//        assertEquals(v1[0], 1)
//        assertEquals(v1[1], 2)
//        assertEquals(v1[2], 3)
//        assertEquals(v1[3], 4)
//        assertEquals(v1[4], 5)
//    }
//
//    @Test
//    fun testTakeLast() {
//        val v = vec.util._v[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
//        val v1 = v.takeLast(5)
//        assertEquals(v1.size, 5)
//        assertEquals(v1[0], 6)
//        assertEquals(v1[1], 7)
//        assertEquals(v1[2], 8)
//        assertEquals(v1[3], 9)
//        assertEquals(v1[4], 10)
//    }
//}
//
//
